<h3>Escriu un enter, un real, i un caràcter per pantalla.</h3>
El meu codi es:

Declaració de variables
    
    int enter,enter2;
    double realDoblePrecisio;
    char caracter;

Inicialització
    
    enter=99;
    realDoblePrecisio= 3.14;
    caracter='a';



Mostrar per pantalla
    
    printf("enter: %i, doble:%lf, Caràcter: %c\n",enter,realDoblePrecisio,caracter);
![Image text](Exercici.png)
