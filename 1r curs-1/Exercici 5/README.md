<h1>Exercici 5</h1>

<h3>Feu un programa amb la següent sortida a pantalla:</h3>

<h3>El número 77 en octal</h3>

<h3>El número 65535 en hexadecimal i en majúscules.</h3>

<h3>El número 32727 en hexadecimal i en minúscules.</h3>

<h3>El número 7325 amb el signe.</h3>

<h3>El número 6754 amb deu dígits i farciment de 0s per l’esquerra.</h3>

<h3>El número 456.54378 amb 3 decimals.</h3>

<h3>El teu nom i cognoms.</h3>

<h3>Els primers 8 caràcters de la sortida del punt anterior.</h3>

<h3>Afegiu una captura de la sortida a pantalla.</h3>

![Image text](Selecció_001.png)
