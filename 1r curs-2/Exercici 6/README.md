<h3>Mostrar per pantalla la suma i el producte dels 5 primers nombres naturals.</h3>

Aquest es el meu codi:

printf("La suma: %i\n",1+2+3+4+5);
printf("El producte: %i\n",1*2*3*4*5);

Suma i fa el producte amb el print al ser nombres enters es fique %i

![Image text](Exercici.png)
