<h3>Quins són els tipus de dades simples en C?</h3>

Feu un programa que escrigui a pantalla un valor per cada un dels
tipus simples. En els tipus numèrics no heu d’incloure els modificadors
short, long, signed i unsigned.

![Image text](Captura3.PNG)

Explica i posa un exemple de desbordament d’una variable de tipus
enter? Afegiu una captura de la sortida a pantalla.

Un desbordament d'una variable de tipus enter és: que quan pases del limit passe a ser negatiu perquè el negatiu té un numero més que el positiu
