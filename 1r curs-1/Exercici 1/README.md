<h1>Exercici 1</h1>

<h3>Cerca al mercat actual un IDE propietari i un de codi lliure on es pugui
programar en ANSI C.</h3>


Propietari:
IBM Rational Application Developer (RAD).

Codi lliure:
Codeblocks.

<h3>Comenta, a grans trets, les característiques de cada un d’ells.</h3>

IBM:
Integració amb el servidor d'aplicacions d'IBM Websphere.
Eines per a desenvolupar, implementar i provar aplicacions en WebSphere Application Server.

Codeblocks:
Espais de treball (workspaces) per a combinar múltiples projectes.
Espai de treball adaptable (canvia segons la tasca que s'estigui realitzant o com es configuri).

<h3>Creus que el CodeBlocks és un bon IDE per programar en C? Raona la
resposta.</h3>

Si, primer punt perque és un programa IDE lliure, segon punt per la facilitat per acabar de completar les paraules del codi,
tercer punt perque et deixe seleccionar amb quin model vols fer el projecte, aixo per un usuari nou va molt bé, a part es molt intuïtiu.
