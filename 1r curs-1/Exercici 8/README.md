<h1>Exercici 8</h1>

<h3>Cita en forma de taula els operadors relacionals, els aritmètics i els lògics que s’utilitzen en ANSI C.</h3>

| Relacional | Significat |
| --- | --- |
| Operador < | Menor que |
| Operador <= | Menor o igual que |
| Operador > | Major que |
| Operador >= | Major o igual que |
| Operador == | Igual a |
| Operador != | Diferent o no igual que |

| Logics | Significat |
|---|---|
|Operador &&|Operador logic and|
|Operador amb dos barretes (no hem deixe ficar-ho) |Operador logic or|
|Operador !|Operador logic not|

| Aritmetics | Significat |
|---|---|

| + |suma|

| - |resta|

| * |producte|

| / |divisió|

| % | resto de la divisió sencera|



<h3>Què és la preferència dels operadors? Posa’n exemples.</h3>

La preferencia és lo que afecta a l'agrupacio i evaluació dels operands en les expresions

