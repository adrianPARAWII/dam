<h3>Demana dos enters i intercanvia els valors de les variables.</h3>

Amb aquest exercici tindrem que fer una variable més que guardara amb aquest cas "a".

Primer de tot demanem a i b, canviem "a" a "c", despres "b" a "a" i despres "c" a "b" aixo se li diu swap

El codi és:

int a,b,c;

    printf("Introdueix el numero a:");
        scanf("%i",&a);

    printf("\nIntrodueix el numero b:");
        scanf("%i",&b);

//swap
c= a;
a= b;
b= c;

    printf("\nEl numero de b ara és: %i",a);
    printf("\nEl numero de b ara és: %i",b);

![Image text](Exercici.png)
