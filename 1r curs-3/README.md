<h3>1. Justifica la utilització de les estructures iteratives que coneixes i posa’n exemples.</h3>

Com a estructures iteratives tenim els següents:

While, for i do

<h3>Per què utilitzare el While ?</h3>

El while l'utilitzarem quan volem que faigui una accio fins que es completi la dada que li hem demanat.

Per exemple:

while(maxim > 100){

printf("ERROR!!!!");

}

que signifique:

mentre(maxim sigui major a 100){

dibuixa a la consola ("ERROR!!!!");

}
<h3>Per què utilitzare el for ?</h3>

Utilitzarem el for fins a que l'instruccio sigui false.

Per exemple:

  for (i = 1; i < 11; ++i)
  
  {
   
    printf("%d ", i);
 
  }
  
  return 0;
  
}

Que signifique:

for (si i és = a 1; i és més < que 11; sumali 1 a i)

{

printf("%d ", i);

}

<h3>Per què utilitzare el do ?</h3>

L'utilitzarem com el While pero quan sapiguem el numero de vegades que farem l'acció.

Per exemple:

double numero, sum = 0;



  do {
  
    printf("Fica el numero: ");
  
    scanf("%lf", &numero);
  
    sum += numero;
  
  }
  
  while(numero != 0.0);

  printf("Sum = %.2lf",sum);


Que signifique:

do{

pinta("Fica el numero: ");

fes un scan("%lf", &numero);

sum += numero;

}

mentres (numero sigui diferent o igual a 0.0);

pinta(sum = %.2lf que es el numero més dos decimals",sum);

2.

![Image text](Exercici2.png)

3.

![Image text](Exercici3.png)

4.

![Image text](Exercici4.png)

5.

![Image text](Exercici5.png)

6.

![Image text](Exercici6.png)

7.

![Image text](Exercici7.png)

8.

![Image text](Exercici8.png)

9.

![Image text](Exercici9.png)

10.

![Image text](Exercici10.png)

12.

![Image text](Exercici12.png)

13.

![Image text](Exercici13.png)

14.

![Image text](Exercici14.png)

15.

![Image text](Exercici15.PNG)

16.

![Image text](Exercici16.PNG)

17.

![Image text](Exercici17.PNG)

18.

![Image text](Exercici18.PNG)

19.

![Image text](Exercici19.PNG)

20.

![Image text](Exercici20.PNG)

21.

![Image text](Exercici21.PNG)
