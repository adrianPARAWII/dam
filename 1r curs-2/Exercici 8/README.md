<h3>Mostra per pantalla el menú i selecciona 1-4</h3>

El meu codi es aquest:

int opcio;
    printf(" 1-Alta\n 2-Baixa\n 3-Modificacions\n 4-Sortir\n Tria opció (1-4): ");
    scanf("%i", &opcio);
    if(opcio==1) printf("Has escollit l'opció Alta");
    else if(opcio==2) printf("Has escollit l'opció Baixa");
    else if(opcio==3) printf("Has escollit l'opció Modificacions");
    else if(opcio==4) printf("Has escollit l'opció Sortir");
    else printf("ERROR!!!! solament hi han opcions de 1 al 4");
    return 0;
}

He creat la variable opcio amb enters i he ficat if si es opcio 1 directament no fara les demes i el else if es per a que faigui lo mateix amb la 2 i totes.

![Image text](Exercici.png)
