<h3>Joc dels daus</h3>
El meu codi es:

srand(getpid());

    int area;
    int numeroElegit;

    area = rand() % 6 + 1;

    printf("Eligeix un numero del 1 al 6 per sapiguer que has encertat: ");
    scanf("%i",&numeroElegit);

    while (numeroElegit<=0) {printf("solament pots elegir un numero del 1 al 6\n");
    printf("Eligeix un numero del 1 al 6 per sapiguer que has encertat: ");
    scanf("%i",&numeroElegit);}

    while (numeroElegit>6) {printf("solament pots elegir un numero del 1 al 6\n");
printf("Eligeix un numero del 1 al 6 per sapiguer que has encertat: ");
    scanf("%i",&numeroElegit);}

    printf("La primera tirada a donat: %i\n",area);

   if(area==numeroElegit) printf("Molt bé has encertat amb el numero: %i",area);

Lo que fa aquest codi és: genere un numero aleatori entre 1 i 6, que si encertes el numero que has ficat amb el que ell genere et felicitara, si fiques un numero més alt de 6 done error i et torne a ferlo fer, i si es 0 o negatiu et fara repetirlo també, llavors solament pots dicar un numero enttre el 1 i el 6.

![Image text](Exercici.png)
